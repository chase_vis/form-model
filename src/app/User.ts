export class User {
    name: {
        firstname: string;
        lastname: string;
    };
    gender: string;
    email: string;
    phonenumber: number;
    address: {
        address1: string;
        address2: string;
        city: string;
        state: string;
        pincode: number;
    };
    description: string;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
