import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-gender',
  templateUrl: './gender.component.html',
  styleUrls: ['./gender.component.css']
})
export class GenderComponent implements OnInit {
  private genders: string[];
  @Input() gender: FormControl;
  constructor() { }

  ngOnInit() {
    // this.genders =  ['Male', 'Female', 'Others'];
  }

}
