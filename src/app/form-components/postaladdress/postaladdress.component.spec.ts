import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostaladdressComponent } from './postaladdress.component';

describe('PostaladdressComponent', () => {
  let component: PostaladdressComponent;
  let fixture: ComponentFixture<PostaladdressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostaladdressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostaladdressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
