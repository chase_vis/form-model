import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { User } from './User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private user: User;
  myForm: FormGroup;


  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.myForm = this.fb.group({
      name: this.fb.group({
        firstname: [''],
        lastname: [''],
      }),
      gender: [''],
      email: [''],
      phonenumber: [''],
      address: this.fb.group({
        address1: [''],
        address2: [''],
        city: [''],
        state: ['Andhra Pradesh'],
        pincode: [''],
      }),
      description: [''],
    });
  }
  public onSubmit() {
    if (this.myForm.valid) {
        this.user = this.myForm.value;
        console.log(this.user);
        this.myForm.reset();
    }
  }
}
