import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NameComponent } from './form-components/name/name.component';
import { GenderComponent } from './form-components/gender/gender.component';
import { EmailComponent } from './form-components/email/email.component';
import { PhonenumberComponent } from './form-components/phonenumber/phonenumber.component';
import { PostaladdressComponent } from './form-components/postaladdress/postaladdress.component';
import { DescriptionComponent } from './form-components/description/description.component';

@NgModule({
  declarations: [
    AppComponent,
    NameComponent,
    GenderComponent,
    EmailComponent,
    PhonenumberComponent,
    PostaladdressComponent,
    DescriptionComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
